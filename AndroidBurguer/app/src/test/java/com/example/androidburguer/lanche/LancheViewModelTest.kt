package com.example.androidburguer.lanche

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidburguer.TestContextProvider
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor
import com.example.androidburguer.factory.LanchesFactory
import com.example.androidburguer.utils.FlowState
import com.example.androidburguer.view.ui.lanche.LancheViewModel
import com.example.androidburguer.view.ui.lanche.LancheViewState
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.lang.Exception
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class LancheViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val factory = LanchesFactory

    @Mock
    lateinit var interactor: LancheInteractor
    lateinit var lancheViewModel: LancheViewModel


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        lancheViewModel = LancheViewModel(interactor, TestContextProvider())
    }

    private fun mockGetLanchesResponse(response: Result<MutableList<Lanche>>) {
        runBlocking {
            `when`(interactor.mostrarLanches()).thenReturn(response)
        }
    }

    @Test
    fun `getLanches WHEN is null MUST return a error message`() {
        runBlocking {
            val expectedException = Exception()

            whenever(interactor.mostrarLanches()).thenReturn(Result.Error(expectedException))

            lancheViewModel.lancheState.postValue(FlowState(LancheViewState.ERROR))

            assertEquals(LancheViewState.ERROR, lancheViewModel.lancheState.value?.status)
        }
    }

    @Test
    fun `getLanches WHEN is success MUST return a success message`() {
        runBlocking {
            whenever(interactor.mostrarLanches()).thenReturn(Result.Success(mutableListOf()))

            lancheViewModel.lancheState.postValue(FlowState(LancheViewState.SUCCESS))

            assertEquals(LancheViewState.SUCCESS, lancheViewModel.lancheState.value?.status)
        }
    }


//    @Test
//    fun `login execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//            loginInteractor.login(mockLoginBody())
//            verify(loginInteractor, times(1)).login(mockLoginBody())
//        }
//    }

//    @Test
//    fun `getListOfLanche execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//
//            verify(lancheInteractorImpl, Times(1)).mostrarLanches()
//        }
//    }
}