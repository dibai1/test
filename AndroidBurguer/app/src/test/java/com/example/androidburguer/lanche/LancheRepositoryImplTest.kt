package com.example.androidburguer.lanche

import com.example.androidburguer.data.Result
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import com.example.androidburguer.data.RetrofitInstance.BurguerService
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.remote.repository.lanche.LancheRepositoryImpl
import com.example.androidburguer.utils.NetworkStatusTest
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

@RunWith(JUnit4::class)
class LancheRepositoryImplTest {

    private lateinit var lancheRepository: LancheRepositoryImpl

    @Mock
    private lateinit var burguerService: BurguerService


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        lancheRepository = LancheRepositoryImpl(burguerService, NetworkStatusTest())
    }

    @Test
    fun `mostrarLanches MUST return Lanches when is successful`(){
        runBlocking {
            doReturn(mockResult()).`when`(burguerService).showLanches()

            val result = lancheRepository.mostrarLanches()

            assert(result is Result.Success)
            verify(burguerService).showLanches()
        }
    }

    @Test
    fun `mostrarError MUST return Error message when is error`(){
        runBlocking {
            doReturn(mockErrorResult()).`when`(burguerService).showLanches()

            val result = lancheRepository.mostrarLanches()

            assert(result is Result.Success)
            verify(burguerService).showLanches()
        }
    }

    private fun mockResult(): Deferred<Lanche> {
        return GlobalScope.async {
            Lanche(21,
                "John Doe",
                mutableListOf(1, 2, 3),
                ""
                )
        }
    }

    private fun mockErrorResult(): Deferred<Lanche> {
        return GlobalScope.async {
            Lanche(-1,
                "",
                mutableListOf(),
                ""
            )
        }
    }
}
