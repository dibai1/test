package com.example.androidburguer.escolherlanche

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidburguer.TestContextProvider
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.interactor.carrinho.CarrinhoInteractor
import com.example.androidburguer.domain.interactor.escolherlanche.EscolherLancheInteractor
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor
import com.example.androidburguer.factory.LanchesFactory
import com.example.androidburguer.utils.FlowState
import com.example.androidburguer.view.ui.escolherlanche.DadosLancheViewState
import com.example.androidburguer.view.ui.escolherlanche.EscolherLancheViewModel
import com.example.androidburguer.view.ui.lanche.LancheViewModel
import com.example.androidburguer.view.ui.lanche.LancheViewState
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.lang.Exception
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class EscolherLancheViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val factory = LanchesFactory

    @Mock
    lateinit var carrinhoInteractor: CarrinhoInteractor

    @Mock
    lateinit var interactor : EscolherLancheInteractor

    lateinit var lancheViewModel: EscolherLancheViewModel


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        lancheViewModel = EscolherLancheViewModel(interactor, carrinhoInteractor, TestContextProvider())
    }

    private fun mockGetLanchesResponse(response: Result<Lanche>) {
        runBlocking {
            `when`(interactor.getDadosLanche(1)).thenReturn(response)
        }
    }

    @Test
    fun `getLanches WHEN is null MUST return a error message`() {
        runBlocking {
            val expectedException = Exception()

            whenever(interactor.getDadosLanche(1)).thenReturn(Result.Error(expectedException))

            lancheViewModel.dadosLancheState.postValue(FlowState(DadosLancheViewState.ERROR))

            assertEquals(DadosLancheViewState.ERROR, lancheViewModel.dadosLancheState.value?.status)
        }
    }

    @Test
    fun `getLanches WHEN is success MUST return a success message`() {
        runBlocking {
            val expectedException = Exception()

            whenever(interactor.getDadosLanche(1)).thenReturn(Result.Success(Lanche(1, "a", mutableListOf(), "")))

            lancheViewModel.dadosLancheState.postValue(FlowState(DadosLancheViewState.SUCCESS))

            assertEquals(DadosLancheViewState.SUCCESS, lancheViewModel.dadosLancheState.value?.status)
        }
    }


//    @Test
//    fun `login execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//            loginInteractor.login(mockLoginBody())
//            verify(loginInteractor, times(1)).login(mockLoginBody())
//        }
//    }

//    @Test
//    fun `getListOfLanche execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//
//            verify(lancheInteractorImpl, Times(1)).mostrarLanches()
//        }
//    }
}