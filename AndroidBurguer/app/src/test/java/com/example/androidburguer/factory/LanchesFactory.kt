package com.example.androidburguer.factory

import com.example.androidburguer.data.entity.Lanche

object LanchesFactory {
    fun mockMutableListLanche(size: Int = 5) = mockLancheListResponse(size)

    fun mockLancheListResponse(size: Int = 5): MutableList<Lanche> {
        val userList = mutableListOf<Lanche>()

        for (i in 1..size) {
            userList.add(mockLancheResponse())
        }

        return userList
    }

    fun mockLancheResponse() = Lanche(1, "a", mutableListOf(),"")


    fun mockLanche() = Lanche(1, "a", mutableListOf(),"")
}