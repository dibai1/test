package com.example.androidburguer.customizar

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.androidburguer.TestContextProvider
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.domain.interactor.ingrediente.IngredienteInteractor
import com.example.androidburguer.factory.LanchesFactory
import com.example.androidburguer.utils.FlowState
import com.example.androidburguer.view.ui.customizar.CustomizarViewModel
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.lang.Exception
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class CustomizarViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val factory = LanchesFactory

    @Mock
    lateinit var interactor : IngredienteInteractor

    lateinit var lancheViewModel: CustomizarViewModel


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        lancheViewModel = CustomizarViewModel(interactor, TestContextProvider())
    }

//    private fun mockGetLanchesResponse(response: Result<MutableList<Ingrediente>>) {
//        runBlocking {
//            `when`(interactor.getIngredientes()).thenReturn(response)
//        }
//    }

//    @Test
//    fun `getLanches WHEN is null MUST return a error message`() {
//        runBlocking {
//            val expectedException = Exception()
//
//            whenever(interactor.getIngredientes()).thenReturn(Result.Error(expectedException))
//
//            lancheViewModel.customizarState.postValue(FlowState(CustomizarViewState.ERROR))
//
//            assertEquals(CustomizarViewState.ERROR, lancheViewModel.customizarState.value?.status)
//        }
//    }
//
//    @Test
//    fun `getLanches WHEN is success MUST return a success message`() {
//        runBlocking {
//            whenever(interactor.getIngredientes()).thenReturn(Result.Success(mutableListOf()))
//
//            lancheViewModel.customizarState.postValue(FlowState(CustomizarViewState.SUCCESS))
//
//            assertEquals(CustomizarViewState.SUCCESS, lancheViewModel.customizarState.value?.status)
//        }
//    }


//    @Test
//    fun `login execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//            loginInteractor.login(mockLoginBody())
//            verify(loginInteractor, times(1)).login(mockLoginBody())
//        }
//    }

//    @Test
//    fun `getListOfLanche execute use case`() {
//        runBlocking {
//            lancheViewModel.lancheState
//
//            verify(lancheInteractorImpl, Times(1)).mostrarLanches()
//        }
//    }
}