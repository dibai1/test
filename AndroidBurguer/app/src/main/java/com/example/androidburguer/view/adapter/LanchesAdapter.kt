package com.example.androidburguer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.androidburguer.R
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.mapper.IngredienteMapper
import com.example.androidburguer.domain.mapper.PrecoMapper
import com.example.androidburguer.view.interfaces.LancheClickListener
import com.example.androidburguer.view.ui.loadImageProgress


class LanchesAdapter(var context: Context, lancheClickListener: LancheClickListener) :
    RecyclerView.Adapter<LanchesAdapter.ViewHolder>() {

    lateinit var lanche: Lanche
    var lancheList: MutableList<Lanche> = mutableListOf()

    private val clickListener: LancheClickListener = lancheClickListener

    fun setAdapter(lanches: MutableList<Lanche>) {
        this.lancheList = lanches
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.lanche_item_layout,
            parent, false
        )
        return ViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int {
        return lancheList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lanche: Lanche = lancheList[position]

        holder.bind()

        holder.img.loadImageProgress(lancheList[position].image, R.drawable.burguer)

        holder.txtName.text = lanche.name

        var preco: Double = PrecoMapper.showPrecos(lanche)
        holder.txtIngredientes.text = IngredienteMapper.showIngredientes(lanche)
        holder.txtPreco.text = "R$ $preco"

    }

    inner class ViewHolder(
        itemView: View,
        private val clickListener: LancheClickListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


        lateinit var parentLayout: ConstraintLayout
        lateinit var img: ImageView
        lateinit var txtName: TextView
        lateinit var txtPreco: TextView
        lateinit var txtIngredientes: TextView


        fun bind() {
            parentLayout = itemView.findViewById(R.id.parent_layout)
            parentLayout.setOnClickListener(this)

            img = itemView.findViewById(R.id.img_logo)
            txtName = itemView.findViewById(R.id.txt_nome)
            txtPreco = itemView.findViewById(R.id.txt_preco)
            txtIngredientes = itemView.findViewById(R.id.txt_ingredientes)
        }


        override fun onClick(v: View?) {
            clickListener.clickItem(v, lancheList[adapterPosition])
        }
    }

}