package com.example.androidburguer.view.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidburguer.R
import com.example.androidburguer.domain.model.IngredienteMapped


class CustomizarAdapter(var context: Context) :
    RecyclerView.Adapter<CustomizarAdapter.ViewHolder>() {

    var ingredienteList: MutableList<IngredienteMapped> = mutableListOf()

    fun setAdapter(ingredientes: MutableList<IngredienteMapped>) {
        this.ingredienteList = ingredientes
    }

    fun getList() : MutableList<IngredienteMapped> = ingredienteList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.customizar_item_layout,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ingredienteList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ingrediente: IngredienteMapped = ingredienteList[position]

        holder.bind()

        holder.txtName.text = ingrediente.name

    }

    inner class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView), TextWatcher {


        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            if(!s!!.isEmpty()) {
                val num = s.toString().toInt()
                ingredienteList[adapterPosition].quantity = num
            }
        }

        lateinit var txtName: TextView
        lateinit var editQuantidade: EditText


        fun bind() {
            txtName = itemView.findViewById(R.id.txt_nome)
            editQuantidade = itemView.findViewById(R.id.edit_quantidade)
            editQuantidade.addTextChangedListener(this)
        }
    }
}