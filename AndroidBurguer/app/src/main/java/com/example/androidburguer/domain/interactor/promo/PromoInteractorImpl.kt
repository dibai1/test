package com.example.androidburguer.domain.interactor.promo

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Promo
import com.example.androidburguer.data.remote.repository.promo.PromoRepository

class PromoInteractorImpl
constructor(
    private val repository: PromoRepository
) : PromoInteractor {

    override suspend fun getPromo(): Result<MutableList<Promo>> {
        return when (val response = repository.getPromo()) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }
}