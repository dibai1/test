package com.example.androidburguer.view.ui.carrinho

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidburguer.R
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.utils.Status
import com.example.androidburguer.view.adapter.CarrinhoAdapter
import com.example.androidburguer.view.interfaces.CarrinhoDeleteClickListener
import kotlinx.android.synthetic.main.activity_carrinho.*
import kotlinx.android.synthetic.main.activity_escolher_lanche.toolbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.widget.Toast
import io.pcyan.sweetdialog.SweetDialog


class CarrinhoActivity : AppCompatActivity(), CarrinhoDeleteClickListener {

    val viewModel: CarrinhoViewModel by viewModel()
    lateinit var adapter: CarrinhoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carrinho)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        adapter = CarrinhoAdapter(this@CarrinhoActivity, this@CarrinhoActivity)

        val linearLayoutManager = LinearLayoutManager(this)

        recycler_carrinho.layoutManager = linearLayoutManager
        recycler_carrinho.adapter = adapter

        viewModel.getItensCarrinho()
    }

    override fun onResume() {
        super.onResume()
        if (!viewModel.observeCustomizarState().hasObservers()) {
            viewModel.observeCustomizarState().observe(this, Observer {
                when (it.status) {
                    Status.LOADING -> onLoading()
                    Status.SUCCESS -> onSuccessLoading(it.data)
                    Status.ERROR -> onError(it.error)
                }
            })
        }
    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: MutableList<LancheCarrinho>?) {
        progress_bar_carrinho.visibility = View.INVISIBLE
        adapter.setAdapter(data!!)
        recycler_carrinho.adapter = adapter
    }

    private fun onLoading() {
        progress_bar_carrinho.visibility = View.VISIBLE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun clickItem(view: View?, lanche: LancheCarrinho) {
        SweetDialog.build()
            .setCancelAble(false)
            .autoCancel(true)
            .setTitle("Deseja excluir esse item?")
            .setContent("Você vai perder esse item permanentemente.")
            .setOnConfirmListener {
                //Problema no BackEnd para deletar
                Toast.makeText(this, "Item Excluido com sucesso!", Toast.LENGTH_SHORT).show()
                viewModel.deleteItemCarrinho(lanche.id)
                adapter.notifyDataSetChanged()
                recycler_carrinho.adapter = adapter
            }
            .setOnCancelListener {
            }
            .showDialog(supportFragmentManager, "normal_dialog")
    }
}
