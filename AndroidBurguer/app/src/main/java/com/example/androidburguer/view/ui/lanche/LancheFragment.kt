package com.example.androidburguer.view.ui.lanche


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidburguer.view.ui.lanche.LancheViewState.*

import com.example.androidburguer.R
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.view.ui.escolherlanche.EscolherLancheActivity
import com.example.androidburguer.view.adapter.LanchesAdapter
import com.example.androidburguer.view.interfaces.LancheClickListener
import kotlinx.android.synthetic.main.fragment_lanches.view.*

import org.koin.androidx.viewmodel.ext.android.viewModel

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

@Suppress("UNREACHABLE_CODE")
class LanchesFragment : Fragment(), LancheClickListener {


    override fun clickItem(view: View?, lanche: Lanche) {
        Log.v("LanchesFragment", "Item Clicked!")
        val intent = Intent(view!!.context, EscolherLancheActivity::class.java)
        intent.putExtra("lancheId", lanche.id)
        startActivity(intent)
    }


    val viewModel: LancheViewModel by viewModel()
    lateinit var adapter : LanchesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view : View =  inflater.inflate(R.layout.fragment_lanches, container, false)

        viewModel.getLanches()

        adapter = LanchesAdapter(context!!,this@LanchesFragment)

        val linearLayoutManager = LinearLayoutManager(view.context)

        view.recyclerView.layoutManager = linearLayoutManager
        view.recyclerView.adapter = adapter

        return view
    }

    override fun onResume() {
        super.onResume()
        if(!viewModel.observeLancheState().hasObservers()) {
            viewModel.observeLancheState().observe(this, Observer {
                when (it.status) {
                    LOADING -> onLoading()
                    SUCCESS -> onSuccessLoading(it.data)
                    ERROR -> onError(it.error)
                }
            })
        }
    }

    private fun onLoading() {
        view!!.p_bar.visibility = View.VISIBLE
    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: MutableList<Lanche>?) {
        view!!.p_bar.visibility = View.INVISIBLE
        adapter.setAdapter(data!!)
        view!!.recyclerView.adapter = adapter
    }
}