package com.example.androidburguer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.androidburguer.R
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.view.interfaces.CarrinhoDeleteClickListener

class CarrinhoAdapter(var context: Context, carrinhoDeleteClickListener: CarrinhoDeleteClickListener) :
    RecyclerView.Adapter<CarrinhoAdapter.ViewHolder>() {

    lateinit var lanche: LancheCarrinho
    var lancheList: MutableList<LancheCarrinho> = mutableListOf()

    private val carrinhoDeleteClickListener: CarrinhoDeleteClickListener = carrinhoDeleteClickListener

    fun setAdapter(lanches: MutableList<LancheCarrinho>) {
        this.lancheList = lanches
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.carrinho_item_layout,
            parent, false
        )
        return ViewHolder(view, carrinhoDeleteClickListener)
    }

    override fun getItemCount(): Int {
        return lancheList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lanche: LancheCarrinho = lancheList[position]

        holder.bind()

        holder.txtName.text = lanche.id.toString()

//        var preco : String = lanche.price
//        holder.txtPreco.text = "R$ $preco"
    }

    inner class ViewHolder(
        itemView: View,
        private val carrinhoDeleteClickListener: CarrinhoDeleteClickListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {



        lateinit var parentLayout: ConstraintLayout
        lateinit var txtName: TextView
        lateinit var txtPreco: TextView
        lateinit var btnExcluir: ImageView


        fun bind() {
            parentLayout = itemView.findViewById(R.id.parent_layout)

            txtName = itemView.findViewById(R.id.txt_nome)
            txtPreco = itemView.findViewById(R.id.txt_preco)
            btnExcluir = itemView.findViewById(R.id.btn_delete_carrinho_item)
            btnExcluir.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            carrinhoDeleteClickListener.clickItem(v, lancheList[adapterPosition])
        }
    }
}