package com.example.androidburguer.data.entity

import com.google.gson.annotations.SerializedName

data class Ingrediente(
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String = "",
    @SerializedName("price") val price : Double,
    @SerializedName("image") val image : String = ""
)