package com.example.androidburguer.base

import android.app.Application
import com.example.androidburguer.data.modules.androidUtilsModels
import com.example.androidburguer.data.modules.interactorModule
import com.example.androidburguer.data.modules.repositoryModule
import com.example.androidburguer.data.modules.viewModelModule
import org.koin.android.ext.android.startKoin

open class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(viewModelModule, interactorModule, repositoryModule, androidUtilsModels))
    }

}