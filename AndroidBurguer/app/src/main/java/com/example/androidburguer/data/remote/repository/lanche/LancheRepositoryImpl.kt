package com.example.androidburguer.data.remote.repository.lanche

import com.example.androidburguer.base.safeApiCall
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.remote.repository.lanche.LancheRepository
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.utils.NetworkStatus
import com.example.androidburguer.utils.NetworkStatusImpl

class LancheRepositoryImpl
constructor(
    private val service: RetrofitInstance.BurguerService,
    private val networkStatus: NetworkStatus
) : LancheRepository {

    override suspend fun mostrarLanches(): Result<MutableList<Lanche>> {
        return safeApiCall(
            { networkStatus.isOnline() },
            { service.showLanches().await() })
    }
}