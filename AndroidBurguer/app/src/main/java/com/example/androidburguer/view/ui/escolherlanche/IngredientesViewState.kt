package com.example.androidburguer.view.ui.escolherlanche

enum class IngredientesViewState {
    LOADING, SUCCESS, ERROR
}