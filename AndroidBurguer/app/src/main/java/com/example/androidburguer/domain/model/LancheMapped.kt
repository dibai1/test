package com.example.androidburguer.domain.model

data class LancheMapped(
    val id : Int,
    val name : String = "",
    val ingredients : MutableList<Int>,
    val ingredientsNames : MutableList<String>,
    val ingredientsObjects : MutableList<IngredienteMapped>,
    val ingredientsOldObjectsNames : MutableList<String>,
    val image : String = "",
    var priceValue : Double,
    var price : String
)