package com.example.androidburguer.base

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.example.androidburguer.data.Result
import retrofit2.HttpException
import java.io.IOException

//suspend fun <T> Deferred<T>.awaitResult(): Result<T> = try {
//    Result.Success(await())
//} catch (e: Exception) {
//    Result.Error(e)
//}

open class DataSourceException(message: String? = null, code: Int? = null) : Exception(message)
class LocalDataNotFoundException : DataSourceException("Data not found in local data source")
class RemoteDataNotFoundException(code: Int? = null) : DataSourceException(null, code)
class NoInternetException(message: String? = "No Internet Connection") : Exception(message)

class ServerException(message: String?) : Exception(message)

suspend fun <T : Any> safeApiCall(
    isOnline: () -> Boolean,
    call: suspend () -> T)
        : Result<T> {
    return when {
        isOnline() -> {
            try {
                val dataFromRemote = call()

                Result.Success(dataFromRemote)
            } catch (httpException: HttpException) {
                Result.Error(RemoteDataNotFoundException(httpException.code()))
            } catch (ioException: IOException) {
                Result.Error(ServerException(ioException.message))
            }
        }
        else -> Result.Error(NoInternetException())
    }
}
