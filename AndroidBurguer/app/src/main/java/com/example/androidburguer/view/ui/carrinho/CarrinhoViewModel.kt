package com.example.androidburguer.view.ui.carrinho

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.domain.interactor.carrinho.CarrinhoInteractor
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.utils.CoroutineContextProvider
import com.example.androidburguer.utils.FlowState
import com.example.androidburguer.utils.Status
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CarrinhoViewModel(private val carrinhoInteractor: CarrinhoInteractor,
                          private val provider: CoroutineContextProvider
) : ViewModel() {


    val carrinhoState : MutableLiveData<FlowState<MutableList<LancheCarrinho>, Status>> = MutableLiveData()

    fun observeCustomizarState(): MutableLiveData<FlowState<MutableList<LancheCarrinho>, Status>> = carrinhoState

    fun getItensCarrinho() {
        GlobalScope.launch(provider.IO) {
            val result = carrinhoInteractor.getItensCarrinho()
            withContext(provider.Main) {
                when(result) {
                    is Result.Success -> {
                        carrinhoState.postValue(FlowState(Status.SUCCESS, result.data))
                    }
                    is Result.Error -> {
                        carrinhoState.postValue(FlowState(Status.ERROR, error = result.exception))
                    }
                }
            }
        }
    }

    fun deleteItemCarrinho(id : Int) {
        GlobalScope.launch(provider.IO) {
            val result = carrinhoInteractor.deleteItemCarrinho(id)
            withContext(provider.Main) {
                when(result) {
                    is Result.Success -> {
                        carrinhoState.postValue(FlowState(Status.SUCCESS, result.data))
                    }
                    is Result.Error -> {
                        carrinhoState.postValue(FlowState(Status.ERROR, error = result.exception))
                    }
                }
            }
        }
    }
}