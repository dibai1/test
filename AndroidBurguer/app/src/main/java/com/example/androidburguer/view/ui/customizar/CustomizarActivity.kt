package com.example.androidburguer.view.ui.customizar

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidburguer.R
import com.example.androidburguer.base.Constants
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.domain.mapper.IngredienteMapper
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.utils.Status
import com.example.androidburguer.view.adapter.CustomizarAdapter
import kotlinx.android.synthetic.main.activity_customizar.*
import kotlinx.android.synthetic.main.activity_customizar.toolbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.ArrayList

class CustomizarActivity : AppCompatActivity() {

    val viewModel: CustomizarViewModel by viewModel()
    lateinit var adapter : CustomizarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customizar)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        adapter = CustomizarAdapter(this)

        val linearLayoutManager = LinearLayoutManager(this)

        recycler_customizar.layoutManager = linearLayoutManager
        recycler_customizar.adapter = adapter

        viewModel.getLanches()
    }

    override fun onResume() {
        super.onResume()
        if(!viewModel.observeCustomizarState().hasObservers()) {
            viewModel.observeCustomizarState().observe(this, Observer {
                when (it.status) {
                    CustomizarViewState.LOADING -> onLoading()
                    CustomizarViewState.SUCCESS -> onSuccessLoading(it.data)
                    CustomizarViewState.ERROR -> onError(it.error)
                    CustomizarViewState.VALID_ING -> onValidIng(it.data)
                    CustomizarViewState.INVALID_ING -> onInvalidIng()
                }
            })
        }
    }

    private fun onInvalidIng() {
        Toast.makeText(this@CustomizarActivity, getString(R.string.valid_quantity_ingredients), Toast.LENGTH_SHORT).show()
    }

    private fun onValidIng(data: MutableList<IngredienteMapped>?) {
        val returnIntent : Intent = Intent()
        returnIntent.putParcelableArrayListExtra(Constants.EXTRA_INGREDIENT_LIST, ArrayList(data!!))
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: MutableList<IngredienteMapped>?) {

        adapter.setAdapter(data!!)
        recycler_customizar.adapter = adapter


        //Pensar em uma nova forma de fazer isso
        btn_finalizar_customizacao.setOnClickListener {
            viewModel.isValidList(adapter.getList())
        }
    }

    private fun onLoading() {
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
