package com.example.androidburguer.view.ui.escolherlanche

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.androidburguer.R
import com.example.androidburguer.base.Constants
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.mapper.IngredienteMapper
import com.example.androidburguer.domain.mapper.PrecoMapper
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.domain.model.LancheData
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.view.adapter.CustomizarAdapter
import com.example.androidburguer.view.adapter.IngredientesAdapter
import com.example.androidburguer.view.ui.HomeActivity
import com.example.androidburguer.view.ui.customizar.CustomizarActivity
import com.example.androidburguer.view.ui.loadImageProgress
import kotlinx.android.synthetic.main.activity_customizar.*
import kotlinx.android.synthetic.main.activity_escolher_lanche.*
import kotlinx.android.synthetic.main.activity_escolher_lanche.toolbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class EscolherLancheActivity : AppCompatActivity() {

    val viewModel: EscolherLancheViewModel by viewModel()

    var preco: Double = 0.0
    var extras = ArrayList<IngredienteMapped>()

    lateinit var adapter: IngredientesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_escolher_lanche)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        var id: Int? = intent.getIntExtra("lancheId", 0)

        viewModel.getDadosLanche(id!!)

        adapter = IngredientesAdapter(this)

        val linearLayoutManager = LinearLayoutManager(this)

        recycler_ingredientes.layoutManager = linearLayoutManager
        recycler_ingredientes.adapter = adapter

        btn_customizar.setOnClickListener {
            val intent = Intent(this@EscolherLancheActivity, CustomizarActivity::class.java)
            startActivityForResult(intent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            Log.i("EscolheLancheActivity", "Activity.RESULT_OK")
            viewModel.onBurguerUpdate(data!!.getParcelableArrayListExtra(Constants.EXTRA_INGREDIENT_LIST))
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
        if (!viewModel.observeDadosLanche().hasObservers()) {
            viewModel.observeDadosLanche().observe(this, Observer {
                when (it.status) {
                    DadosLancheViewState.LOADING -> onLoading()
                    DadosLancheViewState.SUCCESS -> onSuccessLoading(it.data!!)
                    DadosLancheViewState.ERROR -> onError(it.error)
                    DadosLancheViewState.ON_UPDATED -> onUpdated(it.data!!)
                }
            })
        }

        if (!viewModel.observeIngredientes().hasObservers()) {
            viewModel.observeIngredientes().observe(this, Observer {
                when (it.status) {
                    IngredientesViewState.LOADING -> onLoadingIng()
                    IngredientesViewState.SUCCESS -> onSuccessLoadingIng(it.data)
                    IngredientesViewState.ERROR -> onErrorIng(it.error)
                }
            })
        }
    }


    private fun onErrorIng(error: Throwable?) {
    }

    private fun onSuccessLoadingIng(data: Lanche?) {
    }

    private fun onLoadingIng() {
    }

    private fun onLoading() {

    }

    private fun onError(error: Throwable?) {
        error!!.printStackTrace()
    }

    private fun onSuccessLoading(data: LancheMapped) {
        text_nome.text = data.name

        img_main.loadImageProgress(data.image, R.drawable.burguer)

        txt_ingredientes.text = data.ingredientsOldObjectsNames.toString()

        text_preco.text = data.price

        btn_adicionar_carrinho.setOnClickListener {
            //Error no BackEnd para adicionar
            viewModel.addLancheCarrinho(LancheData(data.id, true, IngredienteMapper.makeMutableToArrayInt(data.ingredients), data))
            Toast.makeText(this@EscolherLancheActivity, "Produto adicionado com sucesso ao carrinho!", Toast.LENGTH_SHORT).show()
            val intent = Intent(this@EscolherLancheActivity, HomeActivity::class.java)
            startActivity(intent)
        }
    }

    private fun onUpdated(data: LancheMapped) {
        text_preco.text = data.price
        adapter.setAdapter(viewModel.getNewIngredientes())
        recycler_ingredientes.adapter = adapter
    }

}
