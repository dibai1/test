package com.example.androidburguer.view.ui.customizar

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.interactor.ingrediente.IngredienteInteractor
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.utils.CoroutineContextProvider
import com.example.androidburguer.utils.FlowState
import com.example.androidburguer.utils.Status
import com.example.androidburguer.view.ui.lanche.LancheViewState
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CustomizarViewModel(private val ingredienteInteractor: IngredienteInteractor,
                      private val provider: CoroutineContextProvider
) : ViewModel() {


    val customizarState : MutableLiveData<FlowState<MutableList<IngredienteMapped>, CustomizarViewState>> = MutableLiveData()

    fun observeCustomizarState(): MutableLiveData<FlowState<MutableList<IngredienteMapped>, CustomizarViewState>> = customizarState

    fun getLanches() {
        GlobalScope.launch(provider.IO) {
            val result = ingredienteInteractor.getIngredientes()
            withContext(provider.Main) {
                when(result) {
                    is Result.Success -> {
                        customizarState.postValue(FlowState(CustomizarViewState.SUCCESS, result.data))
                    }
                    is Result.Error -> {
                        customizarState.postValue(FlowState(CustomizarViewState.ERROR, error = result.exception))
                    }
                }
            }
        }
    }

    fun isValidList(lista: MutableList<IngredienteMapped>) {
        if(ingredienteInteractor.isValidList(lista)) {
            customizarState.postValue(FlowState(CustomizarViewState.VALID_ING, lista))
        } else {
            customizarState.postValue(FlowState(CustomizarViewState.INVALID_ING))
        }
    }
}