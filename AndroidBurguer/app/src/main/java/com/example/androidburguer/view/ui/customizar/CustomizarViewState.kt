package com.example.androidburguer.view.ui.customizar

enum class CustomizarViewState {
    LOADING, SUCCESS, ERROR, VALID_ING, INVALID_ING
}