package com.example.androidburguer.domain.mapper

import com.example.androidburguer.data.entity.Lanche

object PrecoMapper {
    fun showPrecos(lanche: Lanche): Double {
        var preco: Double = 0.0
        for (l in lanche.ingredientes) {
            when (l) {
                1 -> {
                    preco += 0.4
                }
                2 -> {
                    preco += 2.0
                }
                3 -> {
                    preco += 3.0
                }
                4 -> {
                    preco += 0.8
                }
                5 -> {
                    preco += 1.5
                }
                6 -> {
                    preco += 1.0
                }
            }
        }
        return preco
    }
}