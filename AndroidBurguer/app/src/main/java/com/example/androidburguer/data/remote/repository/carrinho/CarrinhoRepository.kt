package com.example.androidburguer.data.remote.repository.carrinho

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.domain.model.LancheMapped

interface CarrinhoRepository {
    suspend fun addLancheCarrinho(id: Int) : Result<Lanche>
    suspend fun addLancheCustomizado(id: Int, extras: ArrayList<Int>) : Result<Lanche>
    suspend fun getItensCarrinho() : Result<MutableList<LancheCarrinho>>
    suspend fun deleteItemCarrinho(id : Int) : Result<MutableList<LancheCarrinho>>
}