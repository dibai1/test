package com.example.androidburguer.view.interfaces

import android.view.View
import com.example.androidburguer.data.entity.Lanche

interface LancheClickListener {
    fun clickItem(view : View?, lanche: Lanche)
}
