package com.example.androidburguer.domain.mapper

import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.domain.model.LancheMapped

object LancheMapper {
    fun makePriceFormatted(price: Double): String {
        return String.format("R$ %.2f", price)
    }

    fun makeLancheMapped(lanche: Lanche, list: MutableList<Ingrediente>): LancheMapped {
        val price = IngredienteMapper.makePriceIngredienteList(list)
        return LancheMapped(
            lanche.id,
            lanche.name,
            lanche.ingredientes,
            IngredienteMapper.makeStringMappedList(list),
            IngredienteMapper.makeIngredienteMappedList(list),
            IngredienteMapper.makeIngredienteNameList(IngredienteMapper.makeIngredienteMappedList(list)),
            lanche.image,
            price,
            makePriceFormatted(price)
        )
    }
}