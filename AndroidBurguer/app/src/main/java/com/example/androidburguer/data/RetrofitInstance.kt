package com.example.androidburguer.data

import com.example.androidburguer.data.entity.*
import com.example.androidburguer.domain.model.LancheMapped
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

public class RetrofitInstance {

    val BASE_URL: String = "http://10.7.7.249:8080/api/"

    fun getAPI(): BurguerService {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logInterceptor)

        val retrofit = Retrofit
            .Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client.build())
            .build()

        return retrofit
            .create(BurguerService::class.java)
    }

    interface BurguerService {
        @GET("lanche")
        @Headers("Content-Type: application/json")
        fun showLanches(): Deferred<MutableList<Lanche>>

        @GET("lanche/{id}")
        fun getDadosLanche(@Path("id") id: Int): Deferred<Lanche>

        @GET("ingrediente/de/{id}")
        fun getIngredientesById(@Path("id") id: Int): Deferred<MutableList<Ingrediente>>

        @GET("ingrediente")
        fun getIngredientes(): Deferred<MutableList<Ingrediente>>

        @PUT("carrinho/{id}")
        fun addLancheCarrinho(@Path("id") id: Int) : Deferred<Lanche>

        @PUT("carrinho/{id}")
        fun addLancheCustomizado(@Path("id") id: Int, @Body extras: LancheRequest) : Deferred<Lanche>

        @GET("carrinho")
        fun getItensCarrinho() : Deferred<MutableList<LancheCarrinho>>

        @DELETE("carrinho/remove_carrinho/{id}")
        fun deleteItemCarrinho(@Path("id") id: Int) : Deferred<MutableList<LancheCarrinho>>

        @GET("promocao")
        fun getPromo() : Deferred<MutableList<Promo>>
    }
}