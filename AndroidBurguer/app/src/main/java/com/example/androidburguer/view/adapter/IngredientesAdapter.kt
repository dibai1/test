package com.example.androidburguer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidburguer.R
import com.example.androidburguer.domain.model.IngredienteMapped

class IngredientesAdapter(var context: Context) :
    RecyclerView.Adapter<IngredientesAdapter.ViewHolder>() {

    lateinit var ingrediente: IngredienteMapped
    var ingredienteList: MutableList<IngredienteMapped> = mutableListOf()


    fun setAdapter(ingredientes: MutableList<IngredienteMapped>) {
        this.ingredienteList = ingredientes
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.ingrediente_item_layout,
            parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ingredienteList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ingrediente: IngredienteMapped = ingredienteList[position]

        holder.bind()

        holder.txtName.text = ingrediente.name

        holder.txtQuantidade.text = ingrediente.quantity.toString()
    }

    inner class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        lateinit var txtName: TextView
        lateinit var txtQuantidade: TextView


        fun bind() {

            txtName = itemView.findViewById(R.id.txt_nome)
            txtQuantidade = itemView.findViewById(R.id.txt_quantidade)
        }
    }
}