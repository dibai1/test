package com.example.androidburguer.data.remote.repository.promo

import com.example.androidburguer.base.safeApiCall
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.entity.Promo
import com.example.androidburguer.utils.NetworkStatus

class PromoRepositoryImpl
constructor(
    private val service: RetrofitInstance.BurguerService,
    private val networkStatus: NetworkStatus
) : PromoRepository {
    override suspend fun getPromo(): Result<MutableList<Promo>> {
        return safeApiCall(
            {networkStatus.isOnline()},
            {service.getPromo().await()}
        )
    }

}