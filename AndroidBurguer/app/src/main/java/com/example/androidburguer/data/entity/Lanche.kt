package com.example.androidburguer.data.entity

import com.google.gson.annotations.SerializedName

data class Lanche(
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String = "",
    @SerializedName("ingredients") val ingredientes : MutableList<Int>,
    @SerializedName("image") val image : String = ""
)