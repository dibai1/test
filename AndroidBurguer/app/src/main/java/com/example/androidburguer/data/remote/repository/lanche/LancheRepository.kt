package com.example.androidburguer.data.remote.repository.lanche

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.LancheMapped

interface LancheRepository {
    suspend fun mostrarLanches() : Result<MutableList<Lanche>>
}