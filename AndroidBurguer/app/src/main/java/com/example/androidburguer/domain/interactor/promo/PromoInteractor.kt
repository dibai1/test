package com.example.androidburguer.domain.interactor.promo

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Promo

interface PromoInteractor {
    suspend fun getPromo() : Result<MutableList<Promo>>
}