package com.example.androidburguer.data.entity

import com.google.gson.annotations.SerializedName

data class LancheRequest(
    @SerializedName("extras")
    val ingredientes : ArrayList<Int>
)