package com.example.androidburguer.view.ui.escolherlanche

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.LancheData
import com.example.androidburguer.domain.interactor.carrinho.CarrinhoInteractor
import com.example.androidburguer.domain.interactor.escolherlanche.EscolherLancheInteractor
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.utils.CoroutineContextProvider
import com.example.androidburguer.utils.FlowState
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EscolherLancheViewModel(private val escolherLancheInteractor: EscolherLancheInteractor,
                              private val carrinhoInteractor : CarrinhoInteractor,
                              private val provider: CoroutineContextProvider
) : ViewModel() {

    lateinit var lanche : LancheMapped

    //Dados Lanche View State
    val dadosLancheState : MutableLiveData<FlowState<LancheMapped, DadosLancheViewState>> = MutableLiveData()

    fun observeDadosLanche(): MutableLiveData<FlowState<LancheMapped, DadosLancheViewState>> = dadosLancheState

    fun getDadosLanche(idLanche : Int) {
        GlobalScope.launch(provider.IO) {
            val result = escolherLancheInteractor.getDadosLanche(idLanche)
            withContext(provider.Main) {
                when(result) {
                    is Result.Success -> {
                        dadosLancheState.postValue(FlowState(DadosLancheViewState.SUCCESS, result.data))
                        lanche = result.data
                    }
                    is Result.Error -> {
                        dadosLancheState.postValue(FlowState(DadosLancheViewState.ERROR, error = result.exception))
                    }
                }
            }
        }
    }

    //Indredientes View State
    val ingredientesState : MutableLiveData<FlowState<Lanche, IngredientesViewState>> = MutableLiveData()

    fun observeIngredientes(): MutableLiveData<FlowState<Lanche, IngredientesViewState>> = ingredientesState

    fun addLancheCarrinho(idLanche: LancheData) {
        GlobalScope.launch(provider.IO) {
            carrinhoInteractor.addLancheCarrinho(idLanche)
        }
    }

    fun onBurguerUpdate(parcelableArrayListExtra: ArrayList<IngredienteMapped>) {
        dadosLancheState.postValue(FlowState(DadosLancheViewState.ON_UPDATED, escolherLancheInteractor.onBurguerUpdate(parcelableArrayListExtra, lanche)))
    }

    fun getNewIngredientes() : ArrayList<IngredienteMapped> {
        return escolherLancheInteractor.getNewIngredientes()
    }
}