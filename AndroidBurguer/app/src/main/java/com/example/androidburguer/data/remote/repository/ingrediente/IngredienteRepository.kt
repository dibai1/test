package com.example.androidburguer.data.remote.repository.ingrediente

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche

interface IngredienteRepository {
    suspend fun mostrarIngredientes() : Result<MutableList<Ingrediente>>
}