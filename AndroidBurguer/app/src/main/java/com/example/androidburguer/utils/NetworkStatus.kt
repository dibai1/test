package com.example.androidburguer.utils

import java.io.IOException

class NetworkStatusImpl : NetworkStatus {

    override fun isOnline(): Boolean {
        val runtime = Runtime.getRuntime()
        try {
            val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
            val exitValue = ipProcess.waitFor()
            return exitValue == 0
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        return false
    }
}

class NetworkStatusTest : NetworkStatus {
    override fun isOnline(): Boolean {
        return true
    }

}

interface NetworkStatus {
    fun isOnline() : Boolean
}