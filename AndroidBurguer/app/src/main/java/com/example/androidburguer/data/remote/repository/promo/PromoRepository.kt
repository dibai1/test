package com.example.androidburguer.data.remote.repository.promo

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Promo

interface PromoRepository {
    suspend fun getPromo() : Result<MutableList<Promo>>
}