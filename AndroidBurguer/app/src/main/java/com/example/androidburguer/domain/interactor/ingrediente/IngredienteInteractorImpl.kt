package com.example.androidburguer.domain.interactor.ingrediente

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.remote.repository.ingrediente.IngredienteRepository
import com.example.androidburguer.domain.mapper.IngredienteMapper
import com.example.androidburguer.domain.model.IngredienteMapped

class IngredienteInteractorImpl
constructor(
    private val repository: IngredienteRepository
) : IngredienteInteractor {
    override fun isValidList(list: MutableList<IngredienteMapped>): Boolean {
        return !hasNegativeNumber(list) && hasNotOnlyZeros(list)
    }

    fun hasNotOnlyZeros(list : MutableList<IngredienteMapped>) : Boolean {
        for (ingrediente in list) {
            if(ingrediente.quantity != 0) {
                return true
            }
        }
        return false
    }

    fun hasNegativeNumber(list : MutableList<IngredienteMapped>) : Boolean {
        for (ingrediente in list) {
            if(ingrediente.quantity < 0) {
                return true
            }
        }
        return false
    }

    override suspend fun getIngredientes(): Result<MutableList<IngredienteMapped>> {
        return when(val response = repository.mostrarIngredientes()) {
            is Result.Success -> Result.Success(IngredienteMapper.makeIngredienteMappedList(response.data))
            is Result.Error -> response
        }
    }
}