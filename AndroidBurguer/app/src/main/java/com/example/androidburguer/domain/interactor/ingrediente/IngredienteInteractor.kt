package com.example.androidburguer.domain.interactor.ingrediente

import com.example.androidburguer.data.Result
import com.example.androidburguer.domain.model.IngredienteMapped

interface IngredienteInteractor {
    suspend fun getIngredientes() : Result<MutableList<IngredienteMapped>>
    fun isValidList(lista: MutableList<IngredienteMapped>): Boolean
}