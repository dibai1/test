package com.example.androidburguer.data.remote.repository.escolherlanche

import com.example.androidburguer.base.safeApiCall
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.utils.NetworkStatus

class EscolherLancheRepositoryImpl
constructor(
    private val service: RetrofitInstance.BurguerService,
    private val networkStatus: NetworkStatus
) : EscolherLancheRepository {

    override suspend fun getDadosLanche(id: Int): Result<Lanche> {
        return safeApiCall(
            { networkStatus.isOnline() },
            { service.getDadosLanche(id).await() })
    }

    override suspend fun getIngredientes(id: Int): Result<MutableList<Ingrediente>> {
        return safeApiCall(
            { networkStatus.isOnline() },
            { service.getIngredientesById(id).await() })
    }
}