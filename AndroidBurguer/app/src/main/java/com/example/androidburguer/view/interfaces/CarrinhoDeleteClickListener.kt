package com.example.androidburguer.view.interfaces

import android.view.View
import com.example.androidburguer.data.entity.LancheCarrinho

interface CarrinhoDeleteClickListener {
    fun clickItem(view : View?, lanche: LancheCarrinho)
}
