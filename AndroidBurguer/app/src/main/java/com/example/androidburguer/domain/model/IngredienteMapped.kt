package com.example.androidburguer.domain.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("All")
@Parcelize
data class IngredienteMapped(
    var id: Int,
    var name: String,
    var price: Double,
    var image: String,
    var quantity : Int
) : Parcelable