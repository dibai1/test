package com.example.androidburguer.domain.interactor.lanche

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche

interface LancheInteractor {
    suspend fun mostrarLanches() : Result<MutableList<Lanche>>
}