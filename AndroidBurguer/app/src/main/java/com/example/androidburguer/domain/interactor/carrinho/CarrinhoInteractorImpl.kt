package com.example.androidburguer.domain.interactor.carrinho

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.domain.model.LancheData
import com.example.androidburguer.data.remote.repository.carrinho.CarrinhoRepository

class CarrinhoInteractorImpl
constructor(
    private val repository: CarrinhoRepository
) : CarrinhoInteractor {
    override suspend fun deleteItemCarrinho(id : Int): Result<MutableList<LancheCarrinho>> {
        return when (val response = repository.deleteItemCarrinho(id)) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }

    override suspend fun getItensCarrinho(): Result<MutableList<LancheCarrinho>> {
        return when (val response = repository.getItensCarrinho()) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }

    override suspend fun addLancheCarrinho(data: LancheData): Result<Lanche> {
        return if (data.isCustomizavel) {
            addLancheCarrinho(data.id)
        } else {
            addLancheCustomizado(data.id, data.ingredientes)
        }
    }

    suspend fun addLancheCarrinho(id: Int): Result<Lanche> {
        return when (val response = repository.addLancheCarrinho(id)) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }

    suspend fun addLancheCustomizado(id: Int, extras: ArrayList<Int>): Result<Lanche> {
        return when (val response = repository.addLancheCustomizado(id, extras)) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }
}