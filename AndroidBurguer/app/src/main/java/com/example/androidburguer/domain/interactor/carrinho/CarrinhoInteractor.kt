package com.example.androidburguer.domain.interactor.carrinho

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.domain.model.LancheData
import com.example.androidburguer.domain.model.LancheMapped

interface CarrinhoInteractor {
    suspend fun addLancheCarrinho(data: LancheData) : Result<Lanche>
    suspend fun getItensCarrinho() : Result<MutableList<LancheCarrinho>>
    suspend fun deleteItemCarrinho(id : Int) : Result<MutableList<LancheCarrinho>>
}