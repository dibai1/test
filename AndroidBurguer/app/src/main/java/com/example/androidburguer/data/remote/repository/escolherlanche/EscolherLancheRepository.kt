package com.example.androidburguer.data.remote.repository.escolherlanche

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.LancheMapped

interface EscolherLancheRepository {
    suspend fun getDadosLanche(id: Int): Result<Lanche>
    suspend fun getIngredientes(id: Int): Result<MutableList<Ingrediente>>
}