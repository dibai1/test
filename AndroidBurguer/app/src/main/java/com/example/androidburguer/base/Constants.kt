package com.example.androidburguer.base

object Constants {
    val EXTRA_INGREDIENT_LIST : String = "extra"
    val CARNE_ID : Int = 3
    val QUEIJO_ID : Int = 5
    val ALFACE_ID : Int = 1
    val BACON_ID : Int = 2
}