package com.example.androidburguer.view.ui

import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide

fun ImageView.loadImageProgress(url : String, errorImage : Int) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()

    Glide.with(context)
        .load(url)
        .fitCenter()
        .placeholder(circularProgressDrawable)
        .error(errorImage)
        .into(this)
}