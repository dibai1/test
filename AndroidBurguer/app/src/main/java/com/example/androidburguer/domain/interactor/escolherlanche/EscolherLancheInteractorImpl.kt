package com.example.androidburguer.domain.interactor.escolherlanche

import com.example.androidburguer.base.Constants
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.remote.repository.escolherlanche.EscolherLancheRepository
import com.example.androidburguer.domain.mapper.IngredienteMapper
import com.example.androidburguer.domain.mapper.LancheMapper
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.domain.model.LancheMapped

class EscolherLancheInteractorImpl
constructor(
    private val repository: EscolherLancheRepository
) : EscolherLancheInteractor {

    var newData = ArrayList<IngredienteMapped>()

    override fun getNewIngredientes(): ArrayList<IngredienteMapped> {
        return newData
    }

    override fun onBurguerUpdate(data: ArrayList<IngredienteMapped>, lanche: LancheMapped): LancheMapped {
        val newPrice = calcPrice(data, lanche)
        lanche.priceValue = newPrice
        lanche.price = LancheMapper.makePriceFormatted(newPrice)
        return updateSnack(lanche, data)
    }

    fun calcPrice(data: ArrayList<IngredienteMapped>, lanche : LancheMapped): Double {
        var price: Double = lanche.priceValue
        for (ingredient in data) {
            //Carne Extra
            if (ingredient.quantity > 2 && ingredient.id == Constants.CARNE_ID) {
                price += (ingredient.quantity - (ingredient.quantity / 3)) * ingredient.price
            }
            //Queijo Extra
            else if (ingredient.quantity > 2 && ingredient.id == Constants.QUEIJO_ID) {
                price += (ingredient.quantity - (ingredient.quantity / 3)) * ingredient.price
            } else {
                price += ingredient.price * ingredient.quantity
            }
        }

        //Light
        if (!containIngredient(data, Constants.BACON_ID) && containIngredient(data, Constants.ALFACE_ID)) {
            price *= 0.9
        }

        return price
    }

    fun containIngredient(data : ArrayList<IngredienteMapped>, id : Int) : Boolean {
        for (ingredient in data) {
            if (ingredient.quantity > 0 && ingredient.id == id) {
                return true
            }
        }
        return false
    }

    fun updateSnack(snack: LancheMapped, data: ArrayList<IngredienteMapped>): LancheMapped {
        snack.ingredients.addAll(IngredienteMapper.makeIdMappedList(data))
        snack.ingredientsNames.addAll(IngredienteMapper.makeStringMappedList(data))
        snack.ingredientsOldObjectsNames.addAll(IngredienteMapper.makeIngredienteNameList(data))
        var newData = ArrayList<IngredienteMapped>()
        for (i in data) {
            if (i.quantity == 0) {
            } else {
                newData.add(i)
            }
        }
        this.newData = newData
        snack.ingredientsObjects.addAll(newData)
        return snack
    }

    override suspend fun getDadosLanche(id: Int): Result<LancheMapped> {
        return when (val response = repository.getDadosLanche(id)) {
            is Result.Success -> fillBurguerInfo(response.data)
            is Result.Error -> response
        }
    }

    suspend fun fillBurguerInfo(data: Lanche): Result<LancheMapped> {
        val result = getIngredientes(data.id)
        return when (result) {
            is Result.Success -> {
                return Result.Success(
                    LancheMapper.makeLancheMapped(data, result.data)
                )
            }
            is Result.Error -> result
        }
    }

    override suspend fun getIngredientes(id: Int): Result<MutableList<Ingrediente>> {
        return when (val response = repository.getIngredientes(id)) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }
}