package com.example.androidburguer.domain.interactor.escolherlanche

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.IngredienteMapped
import com.example.androidburguer.domain.model.LancheMapped

interface EscolherLancheInteractor {
    suspend fun getDadosLanche(id : Int) : Result<LancheMapped>
    suspend fun getIngredientes(id : Int) : Result<MutableList<Ingrediente>>
    fun onBurguerUpdate(parcelableArrayListExtra: ArrayList<IngredienteMapped>, lanche: LancheMapped) : LancheMapped
    fun getNewIngredientes(): ArrayList<IngredienteMapped>
}