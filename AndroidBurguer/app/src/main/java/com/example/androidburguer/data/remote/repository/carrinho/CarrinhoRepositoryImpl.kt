package com.example.androidburguer.data.remote.repository.carrinho

import com.example.androidburguer.base.safeApiCall
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.entity.LancheCarrinho
import com.example.androidburguer.data.entity.LancheRequest
import com.example.androidburguer.domain.model.LancheMapped
import com.example.androidburguer.utils.NetworkStatus

class CarrinhoRepositoryImpl
constructor(
    private val service: RetrofitInstance.BurguerService,
    private val networkStatus: NetworkStatus
) : CarrinhoRepository {
    override suspend fun deleteItemCarrinho(id : Int): Result<MutableList<LancheCarrinho>> {
        return safeApiCall(
            {networkStatus.isOnline()},
            {service.deleteItemCarrinho(id).await()}
        )
    }

    override suspend fun getItensCarrinho(): Result<MutableList<LancheCarrinho>> {
        return safeApiCall(
            {networkStatus.isOnline()},
            {service.getItensCarrinho().await()}
        )
    }

    override suspend fun addLancheCarrinho(id: Int) : Result<Lanche> {
         return safeApiCall(
            { networkStatus.isOnline() },
            { service.addLancheCarrinho(id).await() })
    }

    override suspend fun addLancheCustomizado(id: Int, extras: ArrayList<Int>) : Result<Lanche>  {
        return safeApiCall(
            { networkStatus.isOnline() },
            { service.addLancheCustomizado(id, LancheRequest(extras)).await() })
    }
}