package com.example.androidburguer.utils

class FlowState<D, S>(
    val status: S,
    val data: D? = null,
    val error: Throwable? = null
)

enum class Status {
    LOADING, SUCCESS, ERROR
}