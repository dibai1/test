package com.example.androidburguer.domain.model

import com.example.androidburguer.domain.model.IngredienteMapped
import com.google.gson.annotations.SerializedName


data class LancheData(
    val id : Int,
    val isCustomizavel : Boolean,
    val ingredientes : ArrayList<Int>,
    val lancheMapped: LancheMapped
)