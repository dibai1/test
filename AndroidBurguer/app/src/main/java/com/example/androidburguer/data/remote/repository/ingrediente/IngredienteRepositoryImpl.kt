package com.example.androidburguer.data.remote.repository.ingrediente

import com.example.androidburguer.base.safeApiCall
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.remote.repository.lanche.LancheRepository
import com.example.androidburguer.utils.NetworkStatus

class IngredienteRepositoryImpl
constructor(
    private val service: RetrofitInstance.BurguerService,
    private val networkStatus: NetworkStatus
) : IngredienteRepository {

    override suspend fun mostrarIngredientes(): Result<MutableList<Ingrediente>> {
        return safeApiCall(
            { networkStatus.isOnline() },
            { service.getIngredientes().await() })
    }
}