package com.example.androidburguer.domain.interactor.lanche

import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.data.remote.repository.lanche.LancheRepository
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor

class LancheInteractorImpl
constructor(
    private val repository: LancheRepository
) : LancheInteractor {


    override suspend fun mostrarLanches(): Result<MutableList<Lanche>> {
        return when(val response = repository.mostrarLanches()) {
            is Result.Success -> response
            is Result.Error -> response
        }
    }
}