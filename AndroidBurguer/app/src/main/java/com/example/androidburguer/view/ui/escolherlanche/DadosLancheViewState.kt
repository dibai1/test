package com.example.androidburguer.view.ui.escolherlanche

enum class DadosLancheViewState {
    LOADING, SUCCESS, ERROR, ON_UPDATED
}