package com.example.androidburguer.view.ui.lanche

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidburguer.data.Result
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor
import com.example.androidburguer.utils.CoroutineContextProvider
import com.example.androidburguer.utils.FlowState
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LancheViewModel(private val lancheInteractor: LancheInteractor,
                      private val provider: CoroutineContextProvider
) : ViewModel() {


    val lancheState : MutableLiveData<FlowState<MutableList<Lanche>, LancheViewState>> = MutableLiveData()

    fun observeLancheState(): MutableLiveData<FlowState<MutableList<Lanche>, LancheViewState>> = lancheState

    fun getLanches() {
        GlobalScope.launch(provider.IO) {
            val result = lancheInteractor.mostrarLanches()
            withContext(provider.Main) {
                when(result) {
                    is Result.Success -> {
                        lancheState.postValue(FlowState(LancheViewState.SUCCESS, result.data))
                    }
                    is Result.Error -> {
                        lancheState.postValue(FlowState(LancheViewState.ERROR, error = result.exception))
                    }
                }
            }
        }
    }
}