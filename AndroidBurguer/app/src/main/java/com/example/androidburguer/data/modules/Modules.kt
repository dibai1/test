package com.example.androidburguer.data.modules

import com.example.androidburguer.data.RetrofitInstance
import com.example.androidburguer.data.remote.repository.carrinho.CarrinhoRepository
import com.example.androidburguer.data.remote.repository.carrinho.CarrinhoRepositoryImpl
import com.example.androidburguer.data.remote.repository.escolherlanche.EscolherLancheRepository
import com.example.androidburguer.data.remote.repository.escolherlanche.EscolherLancheRepositoryImpl
import com.example.androidburguer.data.remote.repository.ingrediente.IngredienteRepository
import com.example.androidburguer.data.remote.repository.ingrediente.IngredienteRepositoryImpl
import com.example.androidburguer.data.remote.repository.lanche.LancheRepository
import com.example.androidburguer.data.remote.repository.lanche.LancheRepositoryImpl
import com.example.androidburguer.domain.interactor.carrinho.CarrinhoInteractor
import com.example.androidburguer.domain.interactor.carrinho.CarrinhoInteractorImpl
import com.example.androidburguer.domain.interactor.escolherlanche.EscolherLancheInteractor
import com.example.androidburguer.domain.interactor.escolherlanche.EscolherLancheInteractorImpl
import com.example.androidburguer.domain.interactor.ingrediente.IngredienteInteractor
import com.example.androidburguer.domain.interactor.ingrediente.IngredienteInteractorImpl
import com.example.androidburguer.utils.CoroutineContextProvider
import com.example.androidburguer.domain.interactor.lanche.LancheInteractorImpl
import com.example.androidburguer.domain.interactor.lanche.LancheInteractor
import com.example.androidburguer.utils.NetworkStatus
import com.example.androidburguer.utils.NetworkStatusImpl
import com.example.androidburguer.view.ui.carrinho.CarrinhoViewModel
import com.example.androidburguer.view.ui.customizar.CustomizarViewModel
import com.example.androidburguer.view.ui.escolherlanche.EscolherLancheViewModel
import com.example.androidburguer.view.ui.lanche.LancheViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {
    viewModel {
        LancheViewModel(get(), get())
    }

    viewModel {
        EscolherLancheViewModel(get(), get(), get())
    }

    viewModel {
        CustomizarViewModel(get(), get())
    }

    viewModel {
        CarrinhoViewModel(get(), get())
    }
}

val interactorModule = module {
    factory { LancheInteractorImpl(get()) as LancheInteractor }

    factory { EscolherLancheInteractorImpl(get()) as EscolherLancheInteractor }

    factory { CarrinhoInteractorImpl(get()) as CarrinhoInteractor }

    factory { IngredienteInteractorImpl(get()) as IngredienteInteractor }

    factory { CoroutineContextProvider() }
}

val repositoryModule = module {
    factory { LancheRepositoryImpl(get(), get()) as LancheRepository }

    factory { EscolherLancheRepositoryImpl(get(), get()) as EscolherLancheRepository }

    factory { CarrinhoRepositoryImpl(get(), get()) as CarrinhoRepository }

    factory { IngredienteRepositoryImpl(get(), get()) as IngredienteRepository }
}

val androidUtilsModels = module {

    single {
        NetworkStatusImpl() as NetworkStatus
    }

    single {
        RetrofitInstance().getAPI()
    }
}