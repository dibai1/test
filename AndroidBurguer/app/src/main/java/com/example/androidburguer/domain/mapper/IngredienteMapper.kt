package com.example.androidburguer.domain.mapper

import android.widget.EditText
import com.example.androidburguer.data.entity.Ingrediente
import com.example.androidburguer.data.entity.Lanche
import com.example.androidburguer.domain.model.IngredienteMapped


object IngredienteMapper {

    fun makeIngredienteMapped(ingrediente: Ingrediente) : IngredienteMapped{
        return IngredienteMapped(ingrediente.id, ingrediente.name,
            ingrediente.price, ingrediente.image, 0)
    }

    fun makeIngredienteNameList(list : MutableList<IngredienteMapped>) : MutableList<String> {
        val listMapped = mutableListOf<String>()
        for(i in list) {
            listMapped.add(i.name)
        }
        return listMapped
    }

    fun makeIngredienteMappedList(list : MutableList<Ingrediente>) : MutableList<IngredienteMapped> {
        val listMapped = mutableListOf<IngredienteMapped>()
        for(i in list) {
            listMapped.add(makeIngredienteMapped(i))
        }
        return listMapped
    }

    fun makeStringMappedList(list : MutableList<Ingrediente>) : MutableList<String> {
        val listMapped = mutableListOf<String>()
        for(i in list) {
            listMapped.add(i.name)
        }
        return listMapped
    }

    fun makeIdMappedList(list : ArrayList<IngredienteMapped>) : MutableList<Int> {
        val listMapped = mutableListOf<Int>()
        for(i in list) {
            listMapped.add(i.id)
        }
        return listMapped
    }

    fun makeMutableToArrayInt(list : MutableList<Int>) : ArrayList<Int>{
        val listMapped = ArrayList<Int>()
        for(i in list) {
            listMapped.add(i)
        }
        return listMapped
    }

    fun showIngredientes(lanche: Lanche): String {
        val ingredientes = ArrayList<String>()
        for (l in lanche.ingredientes) {
            when (l) {
                1 -> {
                    ingredientes.add("Alface")
                }
                2 -> {
                    ingredientes.add("Bacon")
                }
                3 -> {
                    ingredientes.add("Hamburguer de Carne")
                }
                4 -> {
                    ingredientes.add("Ovo")
                }
                5 -> {
                    ingredientes.add("Queijo")
                }
                6 -> {
                    ingredientes.add("Pão com gergelim")
                }
            }
        }
        return ingredientes.toString()
    }

    fun makePriceIngredienteList(list: MutableList<Ingrediente>): Double {
        var soma : Double = 0.0
        for (i in list) {
            soma += i.price
        }
        return soma
    }

    fun makeStringMappedList(list: ArrayList<IngredienteMapped>): MutableList<String> {
        val listMapped = mutableListOf<String>()
        for(i in list) {
            listMapped.add(i.name)
        }
        return listMapped
    }

}