package com.example.androidburguer.view.ui.lanche

enum class LancheViewState {
    LOADING, SUCCESS, ERROR
}