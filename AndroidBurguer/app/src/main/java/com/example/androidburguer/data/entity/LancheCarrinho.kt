package com.example.androidburguer.data.entity

import com.google.gson.annotations.SerializedName

data class LancheCarrinho(
    @SerializedName("id") val id : Int,
    @SerializedName("id_sandwich") val idSandwich : Int,
    @SerializedName("extras") val extras : MutableList<Int>,
    @SerializedName("date") val date : Long
)